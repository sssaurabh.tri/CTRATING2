// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "CTRATING2",
    platforms: [.iOS(.v13)],
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "CTRATING2",
            targets: ["CTRATING2"]),
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: "CTRATING2", resources: [.process("CTRatingCatlog.xcassets")]),
       
            .testTarget(
            name: "CTRATING2Tests",
            dependencies: ["CTRATING2"]),
    ]
)
